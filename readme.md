# Web Service Demo README.MD

#Project Description
Project contains a controller class having REST service calls with GET, POST and PUT methods. 
1. GET fetches an employee's data by passing id and another call from GET is to fetch all employees.
2. POST is to add an employee.
3. PUT is to delete an employee.

Note: As there is no interaction with DB, so we have to call methods in sequence.


# How to use project


1. Copy URL from Bit bucket and create repository in your local system.
2. Clone it using either git command (git clone <URL>) or by using any interface such as SourceTree, TortoiseGit or SysGit.
3. As all the dependencies are written in pom.xml, build project using maven goal <mvn clean install -DskipTests = true>
4. Now, copy the war file of project and paste it in webapps folder of your local server (e.g. tomcat)
5. To test all the methods, add POSTMAN extension in chrome and hit the URL.