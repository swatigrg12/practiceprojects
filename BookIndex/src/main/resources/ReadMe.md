Problem Statement:
This application is to read files from source and create an index of words by specifying list of pages on which those words are present. All the words must be arranged in alphabetical order.
Few words must not be included while indexing. So, there would be a separate file that would have some words which should not be indexed. While reading all the input pages, that file would also get read and processed accordingly.
If any word is present repeatedly on same page, even then only one entry must be there in output. 

E.g: File 1 			-->		present those and
	 File 2 			-->		days present present
	 exclude_words_file -->		and
	 
	 OutputFile (index.txt) --> days : 1
	 								present : 1,2
	 								those : 1

Approach Followed:
1. First of all, there is a reader class, FileReaderImpl that would read all the files in the specified input folder.  
2. Identify the file which contain words which should not be included while indexing. I have used HashSet<String> to store all the words and later compare it with other words. 
3. Then, there is a ProcessFilesImpl class, that would process all the files. In this class, I have used TreeMap<String, ArrayList<Integer>> to arrange the words alphabetically. To remove all symbols or special characters, regex have been implemented in processInputFile() method.
4. While reading the words, we need to check if that word is already there in the map or not. So, there is a check in processInputFile() method , say dummyMap.get(dummyStr), to get the value of that particular word in the map.
5. After processing all the files and storing data in map, there is a FileWriterImpl class, writeOnFile() method, that writes whole data of map onto that file.
6. To specify path of input, output folder or name of file which contains words that must not be indexed, FileConstants.java file is there. 

To run the application, I have used main method which has object of reader class and later writer class as well.
Main method is in: IndexBook.java class.



 
  