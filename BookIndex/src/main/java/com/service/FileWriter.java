package com.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public interface FileWriter {
	public void writeOnFile(Map<String, ArrayList<Integer>> fileMap) throws IOException ;
}
