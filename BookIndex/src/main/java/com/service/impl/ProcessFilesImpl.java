package com.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.service.ProcessFiles;

public class ProcessFilesImpl implements ProcessFiles {
	int fileNumber = 0;
	Map<String, ArrayList<Integer>> indexBookMap = new TreeMap<>();

	@Override
	public Map<String, ArrayList<Integer>> processInputFile(BufferedReader reader, Set<String> excludeWordsSet)
			throws IOException {

		String line = null;
		fileNumber++;
		while ((line = reader.readLine()) != null) {
			String[] tokens = line.split("[\\s()/\",:@&.?$+-]+");
			for (String token : tokens) {
				token = token.toLowerCase();
				if(excludeWordsSet.contains(token)) {
					continue;
				}
				// checkOccurenceOfWord(token);
				ArrayList<Integer> list = indexBookMap.get(token);
				if (list == null) {
					list = new ArrayList<>();
					list.add(fileNumber);
				} else if (!list.contains(fileNumber)) {
					list.add(fileNumber);
				}
				indexBookMap.put(token, list);

			}
		}

		return indexBookMap;
	}

}
