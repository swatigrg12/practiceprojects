package com.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FilenameUtils;

import com.constants.FileConstants;
import com.service.FileReader;
import com.service.ProcessFiles;

public class FileReaderImpl implements FileReader {

	ProcessFiles processFiles = new ProcessFilesImpl();
	Map<String, ArrayList<Integer>> fileMap = new TreeMap<>();

	public Map<String, ArrayList<Integer>> readFile() throws IOException {
		File inputFiles = new File(FileConstants.INPUT_FILE_LOCATION);
		Set<String> excludeWordSet = new HashSet<>();
		try {
			for (File inputFile : inputFiles.listFiles()) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
				if (inputFile.getName().equalsIgnoreCase(FileConstants.SPECIAL_CHAR_FILE_NAME)) {
					System.out.println("found file");
					excludeWordSet = specialHandlingForFile(reader);
					continue;
				}
				System.out.println(inputFile.getName());
				String extensionName = checkExtension(inputFile.getAbsolutePath());
				fileMap = processFiles.processInputFile(reader, excludeWordSet);
			}
		} catch (IOException exception) {
			exception.getCause();
		}
		return fileMap;
	}

	private static String checkExtension(String fileName) {

		String extensionName = FilenameUtils.getExtension(fileName);
		System.out.println("Extension of file is : " + extensionName);
		return extensionName;
	}
	
	public Set<String> specialHandlingForFile(BufferedReader reader) throws IOException {
		Set<String> wordSet = new HashSet<>();
		String line = null;
		while ((line = reader.readLine()) != null) {
			String[] tokens = line.split("\\s+");
			for (String token : tokens) {
				wordSet.add(token);
			}
		}
		System.out.println(wordSet);
		return wordSet;

	}

}
