package com.service.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Map;

import com.constants.FileConstants;
import com.service.FileWriter;

public class FileWriterImpl implements FileWriter {

	public void writeOnFile(Map<String, ArrayList<Integer>> fileMap) throws IOException {
		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(FileConstants.OUTPUT_FILE_LOCATION)));
		try {
			for (Map.Entry<String, ArrayList<Integer>> map : fileMap.entrySet()) {
				writer.write(map.getKey() + " : " + map.getValue().toString().replace("[", "").replace("]", "") + "\n");
			}
		} catch (IOException ex) {
			ex.getCause();
		} finally {
			writer.close();
		}
	}

}
