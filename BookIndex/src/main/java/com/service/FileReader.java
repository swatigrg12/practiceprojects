package com.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public interface FileReader {
	public Map<String, ArrayList<Integer>> readFile() throws IOException;
	public Set<String> specialHandlingForFile(BufferedReader reader) throws IOException;

}
