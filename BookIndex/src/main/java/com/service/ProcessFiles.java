package com.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public interface ProcessFiles {

	public Map<String, ArrayList<Integer>> processInputFile(BufferedReader reader, Set<String> excludeWordsSet)
			throws IOException;

}
