package com.constants;

public class FileConstants {

	public static final String INPUT_FILE_LOCATION = "src/main/resources/inputfiles";
	public static final String OUTPUT_FILE_LOCATION = "src/main/resources/outputfiles/index.txt";
	public static final String SPECIAL_CHAR_FILE_NAME = "exclude-words.txt";

}
