package com.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.service.FileReader;
import com.service.FileWriter;
import com.service.impl.FileReaderImpl;
import com.service.impl.FileWriterImpl;

public class IndexBook {

	public static void main(String[] args) throws IOException {

		FileReader fileReader = new FileReaderImpl();
		Map<String, ArrayList<Integer>> fileMap = fileReader.readFile();
		FileWriter writer = new FileWriterImpl();
		writer.writeOnFile(fileMap);

	}

}
