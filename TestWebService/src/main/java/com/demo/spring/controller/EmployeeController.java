package com.demo.spring.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.spring.model.Employee;

@Controller
public class EmployeeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

	Map<Integer, Employee> employeeData = new HashMap<Integer, Employee>();

	@RequestMapping(value = EmpRestURIConstants.DUMMY_EMP, method = RequestMethod.GET)
	public @ResponseBody Employee getDummyEmployee() {
		LOGGER.info("Start getDummyEmployee");
		System.out.println("getdummy employee");
		Employee employeeObject = new Employee();
		employeeObject.setEmpId(12345);
		employeeObject.setEmpName("John Cena");
		employeeObject.setCreateDate(new Date());
		employeeObject.setCreateBy("SYSTEM");
		employeeData.put(101, employeeObject);
		return employeeObject;

	}

	@RequestMapping(value = EmpRestURIConstants.GET_EMP, method = RequestMethod.GET)
	public @ResponseBody Employee getEmployee(@PathVariable("id") int empId) {
		LOGGER.info("Start getEmployee. ID= " + empId);
		System.out.println("Start getEmployee. ID= " + empId);
		return employeeData.get(empId);
	}

	@RequestMapping(value = EmpRestURIConstants.GET_ALL_EMP, method = RequestMethod.GET)
	public @ResponseBody List<Employee> getAllEmployees() {
		LOGGER.info("Fetch list of employees");
		List<Employee> empList = new ArrayList<Employee>();
		Set<Integer> empIds = employeeData.keySet();
		for (Integer empId : empIds) {
			empList.add(employeeData.get(empId));
		}

		return empList;
	}

	@RequestMapping(value = EmpRestURIConstants.CREATE_EMP, method = RequestMethod.POST)
	public @ResponseBody String createEmployee(@RequestBody Employee employee) {

		LOGGER.info("Add new employee");
		employee.setCreateDate(new Date());
		try {
			employeeData.put(employee.getEmpId(), employee);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Employee added successfully";

	}

	@RequestMapping(value = EmpRestURIConstants.DELETE_EMP, method = RequestMethod.PUT)
	public @ResponseBody Employee deleteEmployee(@PathVariable("id") int id) {
		LOGGER.info("Deletion of an employee");
		Employee emp = employeeData.get(id);
		employeeData.remove(id);

		return emp;

	}

}